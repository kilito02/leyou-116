package com.leyou.item.client;

import com.leyou.common.vo.PageResult;
import com.leyou.item.dto.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 提供Feign接口
 */
@FeignClient("item-service")
public interface ItemClient {
    @GetMapping("/spu/page")
    public PageResult<SpuDTO> spuPageQuery(
            @RequestParam(value = "page",defaultValue = "1") Integer page,
            @RequestParam(value = "rows",defaultValue = "5") Integer rows,
            @RequestParam(value = "key",required = false) String key,
            @RequestParam(value = "saleable",required = false) Boolean saleable
    );

    @GetMapping("/sku/of/spu")
    public List<SkuDTO> querySkusBySpuId(@RequestParam("id") Long id);

    @GetMapping("/spec/params")
    public List<SpecParamDTO> findSpecParam(@RequestParam(value = "gid",required = false) Long gid,
                                                            @RequestParam(value = "cid",required = false) Long cid,
                                                            @RequestParam(value = "searching",required = false) Boolean searching);

    @GetMapping("/spu/detail")
    public SpuDetailDTO querySpuDetailBySpuId(@RequestParam("id") Long id);

    @GetMapping("/category/list")
    public List<CategoryDTO> findCategoryByIds(@RequestParam("ids") List<Long> categoryIds);

    @GetMapping("/brand/{id}")
    public BrandDTO findBrandById(@PathVariable("id") Long id);

    @GetMapping("/spu/{id}")
    public SpuDTO findSpuById(@PathVariable("id") Long id);

    @GetMapping("/spec/of/category")
    public List<SpecGroupDTO> findSpecGroupByCidWithParams(@RequestParam("id") Long id);

    @GetMapping("/sku/list")
    public List<SkuDTO> findSkusBySkuIds(@RequestParam("ids") List<Long> ids);

    @PutMapping("/stock/minus")
    public Void minusStock(@RequestBody Map<Long,Integer> mapParam);
}
