package com.leyou.item.mapper;

import com.leyou.item.domain.Brand;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface BrandMapper extends Mapper<Brand> {
    void saveCategoryBrand(@Param("bid") Long id,@Param("cids" ) List cids);
    @Select("select * from tb_brand b inner join tb_category_brand cb on b.`id`=cb.`brand_id` where cb.`category_id`=#{id}")
    List<Brand> findBrandByCategoryId(Long id);
}
