package com.leyou.item.service;

import com.leyou.common.exception.ExceptionEnum;
import com.leyou.common.exception.LyException;
import com.leyou.common.utils.BeanHelper;
import com.leyou.item.domain.Category;
import com.leyou.item.dto.CategoryDTO;
import com.leyou.item.mapper.CategoryMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
@Transactional
public class CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;


    public List<CategoryDTO> findCategoryByPid(Long pid) {
        Category category = new Category();
        category.setParentId(pid);
        //得到数据库的中分类列表
        List<Category> categoryList = categoryMapper.select(category);
        if (CollectionUtils.isEmpty(categoryList)){
            throw new LyException(ExceptionEnum.CATEGORY_NOT_FOUND);
        }
        //将数据库中的对象列表转成dto列表
        return BeanHelper.copyWithCollection(categoryList,CategoryDTO.class);
    }

    /**
     * 根据分类id集合获取分类dto集合
     * @param categoryIds
     * @return
     */
    public List<CategoryDTO> findCategoryByIds(List<Long> categoryIds) {
        List<Category> categories = categoryMapper.selectByIdList(categoryIds);
        if (CollectionUtils.isEmpty(categories)){
            throw new LyException(ExceptionEnum.CATEGORY_NOT_FOUND);
        }
        return BeanHelper.copyWithCollection(categories,CategoryDTO.class);
    }
}
