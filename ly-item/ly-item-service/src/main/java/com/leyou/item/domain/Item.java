package com.leyou.item.domain;

import lombok.Data;

@Data
public class Item {
    private Integer id;
    private String name;
    private Long price;
}
