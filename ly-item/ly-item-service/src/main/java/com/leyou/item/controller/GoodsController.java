package com.leyou.item.controller;

import com.leyou.common.vo.PageResult;
import com.leyou.item.domain.SpuDetail;
import com.leyou.item.dto.SkuDTO;
import com.leyou.item.dto.SpuDTO;
import com.leyou.item.dto.SpuDetailDTO;
import com.leyou.item.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品SPU信息
     * @param page
     * @param rows
     * @param key
     * @param saleable
     * @return
     */
    @GetMapping("/spu/page")
    public ResponseEntity<PageResult<SpuDTO>> spuPageQuery(
            @RequestParam(value = "page",defaultValue = "1") Integer page,
            @RequestParam(value = "rows",defaultValue = "5") Integer rows,
            @RequestParam(value = "key",required = false) String key,
            @RequestParam(value = "saleable",required = false) Boolean saleable
    ){
        PageResult<SpuDTO> pageResult=goodsService.spuPageQuery(page, rows, key, saleable);
        return ResponseEntity.ok(pageResult);
    }

    /**
     * 新增商品信息
     * @param spuDTO
     * @return
     */
    @PostMapping("/goods")
    public ResponseEntity<Void> saveGoods(@RequestBody SpuDTO spuDTO){
        goodsService.saveGoods(spuDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 修改商品上下架
     * @param id
     * @param saleable
     * @return
     */
    @PutMapping("/spu/saleable")
    public ResponseEntity<Void> updateSaleable(@RequestParam("id") Long id,
                                               @RequestParam("saleable") Boolean saleable){
        goodsService.updateSaleable(id,saleable);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    /**
     * 根据spuId查询spuDetail回显
     */
    @GetMapping("/spu/detail")
    public ResponseEntity<SpuDetailDTO> querySpuDetailBySpuId(@RequestParam("id") Long id){
           SpuDetailDTO spuDetailDTO=goodsService.querySpuDetailBySpuId(id);
           return ResponseEntity.ok(spuDetailDTO);
    }

    /**
     * 根据spuId查询sku集合信息
     */
    @GetMapping("/sku/of/spu")
    public ResponseEntity<List<SkuDTO>> querySkusBySpuId(@RequestParam("id") Long id){
        List<SkuDTO> list=goodsService.querySkusBySpuId(id);
        return ResponseEntity.ok(list);
    }
    /**
     * 修改商品接口
     *修改商品上下架，更新spu信息，同时需要更新sku
     */
    @PutMapping("/goods")
    public ResponseEntity<Void> updateGoods(@RequestBody SpuDTO spuDTO){
        goodsService.updateGoods(spuDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 根据spuId 查询spu对象
     */
    @GetMapping("/spu/{id}")
    public ResponseEntity<SpuDTO> findSpuById(@PathVariable("id") Long id) {
        SpuDTO spuDTO=goodsService.findSpuById(id);
        return ResponseEntity.ok(spuDTO);
    }
    /**
     * 根据sku集合查询sku对象集合
     */
    @GetMapping("/sku/list")
    public ResponseEntity<List<SkuDTO>> findSkusBySkuIds(@RequestParam("ids") List<Long> ids){
        List<SkuDTO> list=goodsService.findSkusBySkuIds(ids);
        return ResponseEntity.ok(list);
    }

    @PutMapping("/stock/minus")
    public ResponseEntity<Void> minusStock(@RequestBody Map<Long,Integer> mapParam){
        goodsService.minusStock(mapParam);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
