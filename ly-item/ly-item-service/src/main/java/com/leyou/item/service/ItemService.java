package com.leyou.item.service;

import com.leyou.common.exception.ExceptionEnum;
import com.leyou.common.exception.LyException;
import com.leyou.item.domain.Item;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ItemService {

    public Item saveItem(Item item) {
        if (StringUtils.isBlank(item.getName())){
            throw new LyException(ExceptionEnum.INSERT_OPERATION_FAIL);
        }
        return item;
    }
}
