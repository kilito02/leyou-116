package com.leyou.item.controller;

import com.leyou.common.vo.PageResult;
import com.leyou.item.dto.BrandDTO;
import com.leyou.item.dto.CategoryDTO;
import com.leyou.item.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
//@RefreshScope
@RestController
@RequestMapping("/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

//    @Value("${ip}")
    private String ip;

    @GetMapping("/page")
    public ResponseEntity<PageResult<BrandDTO>> brandPageQuery(
            @RequestParam(value = "key",required = false) String key,
            @RequestParam(value = "page",defaultValue = "1") Integer page,
            @RequestParam(value = "rows",defaultValue = "5") Integer rows,
            @RequestParam(value = "sortBy",defaultValue = "id") String sortBy,
            @RequestParam(value = "desc",defaultValue = "false") Boolean desc
    ){
        PageResult<BrandDTO> result=brandService.brandPageQuery(key,page,rows,sortBy,desc);
        return ResponseEntity.ok(result);
    }

    /**
     * @RequestParam可以将都号分隔的字符串直接转成List集合
     * @param brandDTO
     * @param cids
     * @return
     */
    @PostMapping
    public ResponseEntity<Void> saveBrand(BrandDTO brandDTO,@RequestParam("cids") List cids){
        brandService.saveBrand(brandDTO,cids);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    /**
     * 根据品牌id查询品牌
     */
    @GetMapping("/{id}")
    public ResponseEntity<BrandDTO> findBrandById(@PathVariable("id") Long id){
//        System.out.println(ip);
        return ResponseEntity.ok(brandService.findBrandById(id));
    }

    /**
     * 根据分类id查询品牌信息集合
     */
    @GetMapping("/of/category")
    public ResponseEntity<List<BrandDTO>> findBrandByCategoryId(@RequestParam("id") Long id){
        List<BrandDTO> list=brandService.findBrandByCategoryId(id);
        return ResponseEntity.ok(list);
    }
}
