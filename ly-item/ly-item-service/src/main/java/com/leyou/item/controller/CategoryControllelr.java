package com.leyou.item.controller;

import com.leyou.item.dto.CategoryDTO;
import com.leyou.item.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
//@CrossOrigin("http://manage.leyou.com")
public class CategoryControllelr {
    @Autowired
    private CategoryService categoryService;
    @GetMapping("/of/parent")
    public ResponseEntity<List<CategoryDTO>> findCategoryByPid(@RequestParam("pid") Long pid){
        List<CategoryDTO> list=categoryService.findCategoryByPid(pid);
        return ResponseEntity.ok(list);
    }

    /**
     * 根据分类id集合获取分类集合
     * @param categoryIds
     * @return
     */
    @GetMapping("/list")
    public ResponseEntity<List<CategoryDTO>> findCategoryByIds(@RequestParam("ids") List<Long> categoryIds) {
        List<CategoryDTO> list = categoryService.findCategoryByIds(categoryIds);
        return ResponseEntity.ok(list);
    }
}
