package com.leyou.page.listener;

import com.leyou.common.constants.MQConstants;
import com.leyou.page.service.PageService;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PageListener {
    @Autowired
    private PageService pageService;

    /**
     * 新增静态页面
     * 要和发送消息方的参数一摸一样
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(value = MQConstants.Exchange.ITEM_EXCHANGE_NAME, type = ExchangeTypes.TOPIC),//创建交换机
            value = @Queue(value = MQConstants.Queue.PAGE_ITEM_UP, durable = "true"),//指定监听的队列，并创建队列，数据持久化存储
            key = MQConstants.RoutingKey.ITEM_UP_KEY//指定交换机与当前队列的通信规则
    ))
    /**
     * 新增静态页面
     */
    public void addPage(Long id) {
        pageService.createStaticItemPage(id);
    }

    /**
     * 删除静态页面
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(value = MQConstants.Exchange.ITEM_EXCHANGE_NAME,type = ExchangeTypes.TOPIC),
            value = @Queue(value = MQConstants.Queue.PAGE_ITEM_DOWN,durable = "true"),
            key = MQConstants.RoutingKey.ITEM_DOWN_KEY
    ))
    public void delPage(Long id) {
        pageService.deleteStaticPage(id);
    }
}
