package com.leyou.order.mapper;

import com.leyou.common.mapper.BaseMapper;
import com.leyou.order.domain.Order;

public interface OrderMapper extends BaseMapper<Order> {
}
