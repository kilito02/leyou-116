package com.leyou.order.interceptor;

import com.leyou.common.auth.domain.UserHolder;
import com.leyou.common.constants.AuthContants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class OrderInterceptor implements HandlerInterceptor {

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        //获取到用户id
        String userId = request.getHeader(AuthContants.USER_ID);
        if(StringUtils.isBlank(userId)){
            log.info("【购物车模块】获取用户id失败！");
            return false;
        }
        //赋值给UserHolder对象
        UserHolder.setUserId(Long.valueOf(userId));
        //放行
        return true;
    }

}