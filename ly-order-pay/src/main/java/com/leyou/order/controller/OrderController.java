package com.leyou.order.controller;

import com.leyou.order.domain.Order;
import com.leyou.order.dto.OrderDTO;
import com.leyou.order.dto.OrderVO;
import com.leyou.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping
    public ResponseEntity<Long> buildOrder(@RequestBody OrderDTO orderDTO){
        Long orderId = orderService.buildOrder(orderDTO);
        return ResponseEntity.ok(orderId);
    }
    @GetMapping("/{id}")
    public ResponseEntity<OrderVO> queryOrderById(@PathVariable("id") Long id){
        OrderVO orderVO=orderService.queryOrderById(id);
        return ResponseEntity.ok(orderVO);
    }

    @GetMapping("/url/{id}")
    public ResponseEntity<String> createWxPayUrl(@PathVariable("id") Long id){
        String wxPayUrl=orderService.createWxPayUrl(id);
        return ResponseEntity.ok(wxPayUrl);
    }

    @GetMapping("/state/{id}")
    public ResponseEntity<Integer> queryOrderStatus(@PathVariable("id") Long id){
        Integer orderStatus=orderService.queryOrderStatus(id);
        return ResponseEntity.ok(orderStatus);
    }
}
