package com.leyou.order.controller;

import com.leyou.order.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pay")
public class PayController {
    @Autowired
    private OrderService orderService;

    /**
     * 给微信调用，produces指定返回类型
     * @param resp
     * @return
     */
    @PostMapping(value = "/wx/notify",produces = "application/xml")
    public Map<String,String> notifyWxMsg(@RequestBody Map<String,String> resp){
        //判断是否正常接收通知
        orderService.checkWxReturnMsg(resp);
        //如果接受到通知返回结果
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("return_code", "SUCCESS");
        resultMap.put("return_msg", "OK");
        return resultMap;
    }
}
