package com.leyou.cart.controller;

import com.leyou.cart.domain.Cart;
import com.leyou.cart.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping
public class CartController {

    @Autowired
    private CartService cartService;

    /**
     * 添加购物车
     * @param cart
     * @return
     */
    @PostMapping
    public ResponseEntity<Void> addCart(@RequestBody Cart cart){
        cartService.addCart(cart);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     *查询购物车
     */
    @GetMapping("/list")
    public ResponseEntity<List<Cart>> findCartList(){
        List<Cart> list=cartService.findCartList();
        return ResponseEntity.ok(list);
    }

    /**
     *合并购物车
     */
    @PostMapping("/list")
    public ResponseEntity<List<Cart>> addLocalCartToRedis(@RequestBody List<Cart> carts){
        cartService.addLocalCartToRedis(carts);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
