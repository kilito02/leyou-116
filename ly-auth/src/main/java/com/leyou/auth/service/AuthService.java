package com.leyou.auth.service;

import com.leyou.common.auth.domain.UserInfo;
import com.leyou.common.auth.domain.Payload;
import com.leyou.common.auth.utils.JwtUtils;
import com.leyou.common.exception.ExceptionEnum;
import com.leyou.common.exception.LyException;
import com.leyou.common.utils.CookieUtils;
import com.leyou.config.JwtProperties;
import com.leyou.user.client.UserClient;
import com.leyou.user.dto.UserDTO;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class AuthService {
    @Autowired
    private UserClient userClient;

    @Autowired
    private JwtProperties jwtProp;

    @Autowired
    private StringRedisTemplate redisTemplate;

    public void login(String username, String password, HttpServletResponse response) {
        //先验证用户名和密码是否正确
        UserDTO userDTO = userClient.findUsernameAndPassword(username, password);
        if (userDTO == null) {
            throw new LyException(ExceptionEnum.INVALID_USERNAME_PASSWORD);
        }
        //创建载荷使用的用户对象
        UserInfo userInfo = new UserInfo(userDTO.getId(), userDTO.getUsername(), "ROLE_ADMIN");
        writeTokenToCookie(response, userInfo);
    }

    /**
     * 生成token并将token写入cookie
     *
     * @param response
     * @param userInfo
     */
    private void writeTokenToCookie(HttpServletResponse response, UserInfo userInfo) {
        System.out.println(jwtProp);
        //生成token
        String token = JwtUtils.generateTokenExpireInMinutes(userInfo, jwtProp.getPrivateKey(), jwtProp.getUser().getExpire());
        //把token写入浏览器cookie中
        CookieUtils.newCookieBuilder()
                .response(response)
                .name(jwtProp.getUser().getCookieName())
                .value(token)
                .domain(jwtProp.getUser().getCookieDomain())
                .httpOnly(true)
                .build();
    }

    public UserInfo verifyUser(HttpServletRequest request, HttpServletResponse response) {
        //获取cookie
        String token = CookieUtils.getCookieValue(request, jwtProp.getUser().getCookieName());
        if (StringUtils.isBlank(token)) {
            throw new LyException(ExceptionEnum.UNAUTHORIZED);
        }
        //校验token是否合法
        Payload<UserInfo> infoPayload = null;
        try {
            infoPayload = JwtUtils.getInfoFromToken(token, jwtProp.getPublicKey(), UserInfo.class);
        } catch (Exception e) {
            throw new LyException(ExceptionEnum.UNAUTHORIZED);
        }
        //得到token的id
        String payloadId = infoPayload.getId();
        //判断redis中是否有token的id
        if (redisTemplate.hasKey(payloadId)){
            //如果redis中有tokenid说明token已失效了
            throw new LyException(ExceptionEnum.UNAUTHORIZED);
        }

        //获取userInfo
        UserInfo userInfo = infoPayload.getUserInfo();
        //得到当前token的过期时间
        Date expTime = infoPayload.getExpiration();
        //得到刷新的token的时间点
        DateTime refreshTime = new DateTime(expTime).minusMinutes(jwtProp.getUser().getRefreshTime());
        //如果当前时间再刷新时间之后触发刷新token操作
        if (refreshTime.isBefore(System.currentTimeMillis())) {
            //生成token并将token写入Cookie中
            writeTokenToCookie(response,userInfo);
        }
        return userInfo;
    }

    public void logout(HttpServletRequest request, HttpServletResponse response) {
        //获取token
        String token = CookieUtils.getCookieValue(request, jwtProp.getUser().getCookieName());
        //验证token真假
        Payload<UserInfo> payload=null;
        try {
            payload = JwtUtils.getInfoFromToken(token, jwtProp.getPublicKey(),UserInfo.class);
        } catch (Exception e) {
            throw new LyException(ExceptionEnum.UNAUTHORIZED);
        }
        //得到token的id
        String payLoadId = payload.getId();
        //得到过期时间
        Date expDate = payload.getExpiration();
        //查看当前token还有多长时间过期，单位毫秒
        long remianTime=expDate.getTime()- System.currentTimeMillis();
        //如果过期时间超过1秒就放入redis黑名单
        if (remianTime>1){
            redisTemplate.opsForValue().set(payLoadId,"1",remianTime,TimeUnit.MILLISECONDS);
        }
        //删除浏览器端的cookie
        CookieUtils.deleteCookie(jwtProp.getUser().getCookieName(),jwtProp.getUser().getCookieDomain(),response);
    }
}
