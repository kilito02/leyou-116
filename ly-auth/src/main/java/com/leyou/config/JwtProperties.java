package com.leyou.config;

import com.leyou.common.auth.utils.RsaUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.security.PrivateKey;
import java.security.PublicKey;

@Data
@Slf4j
@ConfigurationProperties(prefix = "ly.jwt")
public class JwtProperties {

    private String pubKeyPath;
    private String priKeyPath;

    private PublicKey publicKey;
    private PrivateKey privateKey;

    private JwtUser user=new JwtUser();

    @Data
    public class JwtUser{
        private Integer refreshTime;
        private Integer expire;
        private String cookieName;
        private String cookieDomain;
    }
    @PostConstruct
    public void init() throws Exception {
        publicKey=RsaUtils.getPublicKey(pubKeyPath);
        privateKey=RsaUtils.getPrivateKey(priKeyPath);
    }
}