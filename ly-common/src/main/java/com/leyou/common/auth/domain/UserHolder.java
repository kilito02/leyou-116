package com.leyou.common.auth.domain;

public class UserHolder {
    private static ThreadLocal<Long> TL=new ThreadLocal<>();

    public static void setUserId(Long userId){
        TL.set(userId);
    }

    public static Long getUserId(){
        return TL.get();
    }

    public static void remove(){
        TL.remove();
    }
}
