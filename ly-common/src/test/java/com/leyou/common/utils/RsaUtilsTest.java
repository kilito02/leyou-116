package com.leyou.common.utils;

import com.leyou.common.auth.utils.RsaUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang3.time.FastDateFormat;
import org.junit.Test;
import org.springframework.boot.test.autoconfigure.filter.FilterAnnotations;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;

import static org.junit.Assert.*;

public class RsaUtilsTest {
    private static final String pubPath="D:\\develop\\IDEAProject\\projects\\rsa_key.pub";
    private static final String priPath="D:\\develop\\IDEAProject\\projects\\rsa_key";

    @Test
    public void generateKey() throws Exception {
        RsaUtils.generateKey(pubPath,priPath,"zhangqiang",2048);
    }

    @Test
    public void getPublicKey() throws Exception {
        PublicKey publicKey = RsaUtils.getPublicKey(pubPath);
        System.out.println(publicKey);
    }

    @Test
    public void getPrivateKey() throws Exception {
        PrivateKey privateKey = RsaUtils.getPrivateKey(priPath);
        System.out.println(privateKey);
    }

    @Test
    public void buildJwt() throws Exception {
        //私钥加密
        String jws = Jwts.builder()
                .setSubject("Joe")
                .signWith(RsaUtils.getPrivateKey(priPath))
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis()+11111111))
                .claim("role","admin")
                .compact();
        System.out.println(jws);
    }
    @Test
    public void parseJwt() throws Exception {
        //私钥加密
        String token="eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJKb2UiLCJpYXQiOjE1NzI2OTQzNjcsImV4cCI6MTU3MjcwNTQ3OCwicm9sZSI6ImFkbWluIn0.LOK78Q37KgkA4D2aPB7XcVcAdm0wmtwboO_kCWkiUkwPN19RPec5_RyFLvP_gg9sPikf7fu5kmKQrw6aYhaOVoHjUU8buaR6muC8OcXeoZAhyxgbYr0CUAw-OAAuZU2UY2NsyVFf0RkmdVBIAeCM6NgfPFXajvXyyc33f9DuRCQzFMCSvMnofirywjFR9FeEoDLkD8SaaLj987x95pSOPk8WNSa_BdycCsaXlg8tO5uYvDN1gBsaQwWfvkXMpPxYT9p-CsPE_isobCcHrOcG3Y8YnbkjumIXJPGoVoKl3GftEWsxPZcXvMh6cAjhGCYiBzzjmaonA4vc3iKPs_pCtA";
        Claims claims = Jwts.parser()
                .setSigningKey(RsaUtils.getPublicKey(pubPath))
                .parseClaimsJws(token)
                .getBody();
        System.out.println(claims.getSubject());
        System.out.println(FastDateFormat.getInstance("yyyy-MM-dd HH:ss:mm").format(claims.getIssuedAt()));
        System.out.println(FastDateFormat.getInstance("yyyy-MM-dd HH:ss:mm").format(claims.getExpiration()));
        System.out.println(claims.get("role"));
    }

}