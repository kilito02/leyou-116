package com.leyou.upload.controller;

import com.leyou.upload.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.ws.Response;
import java.util.Map;

@RestController
public class UploadController {
    @Autowired
    private UploadService uploadService;

    @PostMapping("/image")
    public ResponseEntity<String> localFileUpload(@RequestParam("file") MultipartFile file){
        String imageUrl=uploadService.localFileUpload(file);
        return ResponseEntity.ok(imageUrl);
    }
    @GetMapping("/signature")
    public ResponseEntity<Map<String,Object>> getOssSignature(){
        Map<String,Object> map=uploadService.getSignature();
        return ResponseEntity.ok(map);
    }
}
