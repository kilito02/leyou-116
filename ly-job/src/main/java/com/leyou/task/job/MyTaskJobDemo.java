package com.leyou.task.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MyTaskJobDemo {
    private Integer i=0;
//    @Scheduled(fixedDelay = 2000)
    @Scheduled(cron = "0/2 * * * * ?")
    public void delayDemo() throws InterruptedException {
        if (i == 1) {
            Thread.sleep(3000);
        }
        i++;
        log.info("delayDemo test");
    }

    @Scheduled(fixedRate = 2000)
    public void rateDemo() throws InterruptedException {
        if (i == 1) {
            Thread.sleep(3000);
        }
        i++;
        log.info("rateDemo test");
    }
}
