package com.leyou.task.utils;

/**
 * @author 黑马程序员
 */
public interface RedisLock {
    boolean lock(long releaseTime);
    void unlock();
}
