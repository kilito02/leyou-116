package com.leyou.user.client;

import com.leyou.user.dto.AddressDTO;
import com.leyou.user.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("user-service")
public interface UserClient {
    /**
     * 判断用户名和密码是否正确
     */
    @GetMapping("/query")
    public UserDTO findUsernameAndPassword(@RequestParam("username") String username,
                                           @RequestParam("password") String password);
    /**
     * 根据
     * @param userId 用户id
     * @param id 地址id
     * @return 地址信息
     */
    @GetMapping("/address")
    public AddressDTO queryAddressById(@RequestParam("userId") Long userId, @RequestParam("id") Long id);

}
